import { ELNameEnum, ELTypeEnum } from "./ELEnum";
import { ELValidateError } from "./ELValidateError";

// 定义类
export class ELNode {
    //API对接字段
    //类型： idType 或 elType
    type: string;
    //名称： THEN ,WHEN, IF, SWITCH
    name: string;
    //条件节点id,当name 是 IF 或 SWITCH时必填
    conditionNodeId?: string;
    //EL表达式id
    aliasNodeId?: string;
    //数据
    data?: string;
    //节点id, type 是 idType 时，必填`
    nodeId?: string;
    //子节点, type 是elType 时，必填
    child: ELNode[] = new Array();

    //非API对接字段
    code?:string;
    displayName: string;
    parent?: ELNode;
    childIsSwitchOrIf?: boolean;
    switchTips?: string;
    comCss?: boolean;
    thenCss?: boolean;
    switchCss?: boolean;
    ifCss?: boolean;
    hiddenOperate?: boolean;
    showAddCommon?:boolean;
    showAddSwitch?:boolean;
    showAddIf?:boolean;
    showDataInput?:boolean;

    // 构造方法
    constructor(type: string, name: string, displayName: string, child?: ELNode[]) {
        this.type = type;
        this.name = name;
        this.displayName = displayName;
        if (child) {
            this.child = child;
        }

    }

    addChild(node: ELNode) {
        this.child?.push(node)
        console.log("after add.", this)
    }

    //生成api对接数据结构
    genJson() {

        return JSON.stringify(this, [
            "type",
            "name",
            "conditionNodeId",
            "aliasNodeId",
            "data",
            "nodeId",
            "child",
            "code"
        ])

    }

    validate(){
     
    }

}