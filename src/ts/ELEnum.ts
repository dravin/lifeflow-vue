export enum ELNameEnum {
    COM = "COM", THEN = "THEN", WHEN = "WHEN", IF = "IF", SWITCH = "SWITCH"
}

export enum ELTypeEnum {
    elType = "elType", idType = "idType"
}