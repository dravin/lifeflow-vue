export class ELValidateError extends Error {
    constructor(msg: string) {
        super(msg)
        this.name = 'ELParseError'
    }
}