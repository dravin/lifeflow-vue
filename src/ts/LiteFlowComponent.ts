export class LiteFlowComponent {

    //名称： THEN ,WHEN, IF, SWITCH
    name: string;
    //展示名
    displayName: string;
    //节点id
    nodeId: string='';
    //别名节点id
    aliasNodeId: string='';

    script:boolean = false;
    //===============
    //当name = SWITCH/IF 时，这个字段表示选择组件的子组件
    child: LiteFlowComponent[] = new Array();
    //临时把condition放这个
    conditionNodeId?: string;
    //临时把code放这里
    code?: string;




    // 构造方法
    constructor(name: string, displayName: string, nodeId: string, aliasNodeId: string) {

        this.name = name;
        this.displayName = displayName;
        this.nodeId = nodeId;
        this.aliasNodeId = aliasNodeId

    }
}