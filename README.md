# 本项目已废弃

请食用最新的ui方案： [liteflow-logicflow-vue](https://gitee.com/dravin/liteflow-logicflow-vue)

## lifeflow-vue (deprecated)


基于规则引擎liteflow： https://gitee.com/dromara/liteFlow， 开发的前端配置页面，
通过页面配置，生成json或EL表达式

![img](./flow.png)

## 对接步骤 
1. 先把上面前端跑起来，然后配置好流程之后，生成json
2. 后端使用下面这个类接收，调用generateEl()方法就可以解析出EL表达式


后端演示项目：
https://gitee.com/dravin/liteflow-vue-api




```JAVA
package com.qjl.liteflowdemo.parser.pojo;

import com.qjl.liteflowdemo.parser.execption.ExpressLanguageParseException;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.springframework.util.CollectionUtils;
import org.springframework.util.StringUtils;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

/**
 * elnode
 * 表达语言
 *
 * @author zhangrongyan
 * @date 2023/01/13
 */
@Data
@AllArgsConstructor
@NoArgsConstructor
public class ELNode {

    /**
     * 类型,必填
     *
     * @see ELTypeEnum
     */
    String type;
    /**
     * 名字, type 是elType 时，必填
     *
     * @see ELNameEnum
     */
    String name;
    /**
     * 条件节点id
     */
    String conditionNodeId;
    /**
     * 别名节点id
     */
    String aliasNodeId;
    /**
     * 数据
     */
    String data;
    /**
     * 节点id, type 是 idType 时，必填
     */
    String nodeId;
    /**
     * 子节点, type 是elType 时，必填
     */
    List<ELNode> child = new ArrayList<>();

    /**
     * elname枚举
     * 支持 THEN, SWITCH, IF
     * 不支持 WHEN,FOR, WHILE, BREAK
     *
     * @author zhangrongyan
     * @date 2023/01/16
     */
    public enum ELNameEnum {
        THEN, WHEN, SWITCH, IF, FOR, WHILE, BREAK;
    }

    /**
     * eltype枚举
     *
     * @author zhangrongyan
     * @date 2023/01/16
     */
    public enum ELTypeEnum {
        idType, elType;
    }

    /**
     * 添加孩子节点
     *
     * @param node 节点
     */
    public void addChild(ELNode node) {
        child.add(node);
    }

    /**
     * 生成EL表达式字符串
     *
     * @return {@link String}
     * @throws ExpressLanguageParseException 表达语言解析异常
     */
    public String generateEl() throws ExpressLanguageParseException {
        return this.getElString(this);
    }

    /**
     * 验证
     *
     * @param elNode el节点
     * @return {@link Boolean}
     * @throws ExpressLanguageParseException 表达语言解析异常
     */
    public Boolean validate(ELNode elNode) throws ExpressLanguageParseException {
        String type = elNode.getType();
        String name = elNode.getName();
        String nodeId = elNode.getNodeId();
        String conditionNodeId = elNode.getConditionNodeId();
        List<ELNode> children = elNode.getChild();
        if (StringUtils.isEmpty(type)) {
            throw new ExpressLanguageParseException("type 不能为空");
        }
        if (ELTypeEnum.idType.name().equals(type) && StringUtils.isEmpty(nodeId)) {
            throw new ExpressLanguageParseException("type 是 idType ，必填节点id");
        }

        if (ELTypeEnum.elType.name().equals(type)) {
            if (CollectionUtils.isEmpty(children)) {
                throw new ExpressLanguageParseException("type 是 el表达式 时，必填子节点");
            }
            if (StringUtils.isEmpty(name)) {
                throw new ExpressLanguageParseException("type 是 el表达式 时，必填 name");
            }
            List<String> supportName = Arrays.asList(ELNameEnum.THEN.name(),  ELNameEnum.SWITCH.name(), ELNameEnum.IF.name());
            if (!supportName.contains(name)) {
                throw new ExpressLanguageParseException("仅支持 THEN/SWITCH/IF");
            }
            if (ELNameEnum.SWITCH.name().equals(name) || ELNameEnum.IF.name().equals(name)) {
                if (StringUtils.isEmpty(conditionNodeId)) {
                    throw new ExpressLanguageParseException("SWITCH/IF 组件里需要 conditionNodeId ");
                }
            }
        }
        return true;
    }

    /**
     * 生成EL表达式字符串
     *
     * @param elNode el节点
     * @return {@link String}
     * @throws ExpressLanguageParseException 表达语言解析异常
     */
    private String getElString(ELNode elNode) throws ExpressLanguageParseException {
        String name = elNode.getName();
        String conditionNodeId = elNode.getConditionNodeId();
        String aliasNodeId = elNode.getAliasNodeId();
        String data = elNode.getData();
        String nodeId = elNode.getNodeId();
        String type = elNode.getType();
        List<ELNode> children = elNode.getChild();
        // 校验参数
        this.validate(elNode);
        String elStr = nodeId;
        if (ELTypeEnum.elType.name().equals(type)) {
            //遍历子节点，获取EL字符串
            List<String> list = new ArrayList<>();
            for (ELNode child : children) {
                //校验子节点
                this.validate(child);
                String elString;
                if(ELTypeEnum.elType.name().equals(child.getType())){
                    elString = this.getElString(child);
                }else{
                    elString= this.doWithSuffix( child.getNodeId(), child.getAliasNodeId(), child.getData());
                }
                if (!StringUtils.isEmpty(elString)) {
                    list.add(elString);
                }
            }
            elStr = this.elOperate(name, conditionNodeId, list);
        }

        return this.doWithSuffix(elStr, aliasNodeId, data);
    }

    /**
     * 根据不同el操作,拼接EL需要的字符串
     *
     * @param name            名字
     * @param conditionNodeId 条件节点id
     * @param params          参数个数
     * @return {@link String}
     */
    private String elOperate(String name, String conditionNodeId, List<String> params) {

        String elStr = "";
        if (CollectionUtils.isEmpty(params)) {
            return elStr;
        }
        String join = String.join(",", params);

        if (ELNameEnum.THEN.name().equals(name) || ELNameEnum.WHEN.name().equals(name)) {
            elStr = String.format("%s(%s)", name, join);
        }
        if (ELNameEnum.SWITCH.name().equals(name)) {
            elStr = String.format("%s(%s).TO(%s)", name, conditionNodeId, join);
        }
        if (ELNameEnum.IF.name().equals(name)) {
            elStr = String.format("%s(%s,%s)", name, conditionNodeId, join);
        }
        return elStr;
    }


    /**
     * 后缀处理，id和data
     *
     * @param elString    el字符串
     * @param aliasNodeId 别名节点id
     * @param data        数据
     * @return {@link String}
     */
    private String doWithSuffix(String elString, String aliasNodeId, String data) {
        if (StringUtils.isEmpty(elString)) {
            return "";
        }
        if (!StringUtils.isEmpty(data)) {
            elString = String.format("%s.data(%s)", elString, data);
        }
        if (!StringUtils.isEmpty(aliasNodeId)) {
            elString = String.format("%s.id(\"%s\")", elString, aliasNodeId);
        }
        return elString;
    }

}


```


```
package com.qjl.liteflowdemo.parser.execption;

/**
 * @author : zhangrongyan
 * @date : 2023/1/16 10:49
 */
public class ExpressLanguageParseException extends Exception{
    public ExpressLanguageParseException(String message) {
        super(message);
    }
}

```
